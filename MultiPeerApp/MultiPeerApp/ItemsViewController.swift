//
//  ViewController.swift
//  MultiPeerApp
//
//  Created by Quyen Castellanos on 6/16/17.
//  Copyright © 2017 Quyen Castellanos. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class ItemsViewController: UIViewController {

    // IBOutlets
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var connectionsLabel: UILabel!
    @IBOutlet weak var itemsLabel: UILabel!
    
    fileprivate var items:[Item] = []
    fileprivate var realmItems : [Item] {
        guard let realmServer = self.realm else {
            preconditionFailure("No private Realm instantiated")
        }
        
        return Array(realmServer.objects(Item.self)) ?? []
    }
    fileprivate let reuseId:String = "editableCell"
    fileprivate let peerToPeerManager:PeerToPeerServiceManager = PeerToPeerServiceManager()
    fileprivate var realm:Realm?
    
    //IBActions
    @IBAction func addTapped(_ sender: Any) {
        if let input = textField.text {
            let item = Item()
            item.id = input
            let _ = self.addItemToRealm(item: item)
            self.items.append(item)
            
            self.textField.text = ""
            self.itemsTableView.reloadData()
            self.peerToPeerManager.send(item: item)
            
            self.updateRealmItemsUI()
        }
    }
    
    fileprivate func updateRealmItemsUI () {
        guard let realmServer = self.realm else {
            preconditionFailure("No private Realm instantiated")
        }
    
        let realmItems = realmServer.objects(Item.self).map({ $0.id })
        self.itemsLabel.text = realmItems.joined(separator: ", ")
    }
    
    fileprivate func addItemToRealm (item:Item) {
        guard let realmServer = self.realm else {
            preconditionFailure("No private Realm instantiated")
        }
        
        try! realmServer.write {
            realmServer.add(item, update: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.peerToPeerManager.delegate = self
        self.realm = try! Realm()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cellChanged (item:Item, newId:String) {
        // is it mine? update if yes
        guard let realmServer = self.realm else {
            preconditionFailure("No private Realm instantiated")
        }
        
        if let foundRealmItem = realmServer.objects(Item.self).first(where: {$0.objectId == item.objectId}) {
            try! realmServer.write {
                print("Found item in Realm; now updating")
                foundRealmItem.id = newId
                self.updateRealmItemsUI()
            }
        }
        else {
            // otherwise, send out the notification
            item.id = newId
            self.peerToPeerManager.send(item: item)
        }
    }
}

extension ItemsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseId) as? EditableTableViewCell else {
            preconditionFailure("Cell couldn't not be dequeded as an EditableTableViewCell")
        }
        cell.configure(item: items[indexPath.row], delegate: self)
        return cell
    }
}

extension ItemsViewController: PeerToPeerServiceManagerDelegate {
    func connectedDevicesChanged(manager: PeerToPeerServiceManager, connectedDevices: [String]) {
        OperationQueue.main.addOperation {
            self.connectionsLabel.text = "Connections: \(connectedDevices)"
        }
    }
    
    func syncItem(manager: PeerToPeerServiceManager, item: Item) {
        OperationQueue.main.addOperation {
            // Try to find item in my Realm; if found update it
            guard let realmServer = self.realm else {
                preconditionFailure("No private Realm instantiated")
            }
            
            if let foundRealmItem = realmServer.objects(Item.self).first(where: {$0.objectId == item.objectId}) {
                try! realmServer.write {
                    foundRealmItem.id = item.id
                    self.updateRealmItemsUI()
                }
            }
            
            // Add items to array for UI updates but don't add to local realm
            if let itemIndex = self.items.index(where: { $0.objectId == item.objectId }) {
                let newItem:Item = Item()
                newItem.objectId = item.objectId
                newItem.id = item.id
                self.items[itemIndex] = newItem
                self.itemsTableView.reloadData()
            } else {
                self.items.append(item)
                self.itemsTableView.reloadData()
            }
        }
    }
}

