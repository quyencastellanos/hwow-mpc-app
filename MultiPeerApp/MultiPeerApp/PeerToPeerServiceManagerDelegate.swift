//
//  ColorServiceManagerDelegate.swift
//  ConnectedColors
//
//  Created by Quyen Castellanos on 6/16/17.
//  Copyright © 2017 Quyen Castellanos. All rights reserved.
//

import Foundation

protocol PeerToPeerServiceManagerDelegate {
    func connectedDevicesChanged(manager: PeerToPeerServiceManager, connectedDevices: [String])
    func syncItem(manager: PeerToPeerServiceManager, item:Item)
}
