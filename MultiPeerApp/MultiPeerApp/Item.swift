import Foundation
import RealmSwift
import Realm

class Item: Object, NSCoding {
    dynamic var objectId:String = UUID().uuidString
    dynamic var id = ""
    
    override class func primaryKey() -> String? {
        return "objectId"
    }
    
    required init () {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.id = aDecoder.decodeObject(forKey: "id") as? String ?? ""
        self.objectId = aDecoder.decodeObject(forKey: "objectId") as? String ?? ""
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(objectId, forKey: "objectId")
    }
}
