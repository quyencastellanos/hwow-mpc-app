//
//  EditableTableViewCell.swift
//  MultiPeerApp
//
//  Created by Quyen Castellanos on 6/19/17.
//  Copyright © 2017 Quyen Castellanos. All rights reserved.
//

import UIKit

class EditableTableViewCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    
    var item:Item?
    var delegate:ItemsViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.textField.addTarget(self, action: #selector(textFieldEditDone(textField:)), for: .editingDidEnd)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure (item:Item, delegate:ItemsViewController) {
        self.item = item
        self.textField.text = item.id
        self.delegate = delegate
    }
    
    func textFieldEditDone (textField: UITextField) {
        if let text = textField.text, let item = self.item {
            delegate?.cellChanged(item: item, newId: text)
        }
//        if let text = textField.text, let delegate = self.delegate {
//            print("typed \(text)")
//            let newItem = self.getNewItem(newId: text)
//            delegate.cellChanged(modifiedItem: newItem)
//            self.item = newItem
//        }
    }
    
    func getNewItem (newId: String) -> Item {
        guard let item = self.item else {
            preconditionFailure("No Item object found for given UITableViewCell")
        }
        
        let newItem:Item = Item()
        newItem.objectId = item.objectId
        newItem.id = newId
        return newItem
    }
}
